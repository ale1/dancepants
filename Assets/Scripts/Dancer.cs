using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DancePants
{
    
    public class Dancer : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private AudioSource _audioSource;
        

        public void SetSong(AudioClip clip)
        {
            _audioSource.Stop();
            _audioSource.clip = clip;
        }

        public void SetVisibility(float vis)
        {
            if (vis > 72)
            {
                Play();
            }
            else if (vis < 50)
            {
                Pause();
            }
        }

        public void Play()
        {
            if(!_audioSource.isPlaying)
                _audioSource.Play();
            _animator.enabled = true;
        }

        public void Pause()
        {
            if(_audioSource && _audioSource.isPlaying)
                _audioSource.Pause();
            if(_animator)
                _animator.enabled = false;
        }
    }
}