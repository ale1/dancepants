using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace DancePants
{
    
    /// <summary>
    /// This class creates the intial panels and generates 1000 variations of panel content to simulate a "database" of player generated content.
    /// </summary>
    public class Bootstrap : MonoBehaviour
    {
        [Header("must be odd number")] 
        public int PanelBufferSize; // panel buffer size.
        public GameObject contentPanelPrefab;
        public SwipeController swipeManager;

        private RectTransform parentRectTransform;
        private List<ContentPanel> bufferPanels = new List<ContentPanel>();

        void Start()
        {
            if(PanelBufferSize % 2 == 0)
                Debug.LogError("PanelBufferSize must be an odd number!");
        
            // Bootstrap ResourceManager with 1000 fake Dance Sessions  //
            // Bootstrap by creating 1000 ContentDatas.                 //
            for (int i = 0; i < ResourceManager.DanceSessionsCount; i++)
            {
                ResourceManager.DanceSessions[i] = new DanceSession()
                {
                    PlayerId = "ale",
                    AvatarName = "Avatar" + Random.Range(1,4),  //pick avatar at random
                    SongName ="Song" + Random.Range(1,10), //pick son at random
                    StageName = new float[]{Random.Range(0f,1f), Random.Range(0f,1f), Random.Range(0f,1f), 1} // pick background color at random
                };
            }
            
            // Bootstrap the first initial panels                                    //
            // (central focused panel plus  buffers on each side)   //
            parentRectTransform = swipeManager.panelContainer;
            int middleIndex = PanelBufferSize / 2;

            for (int i = 0; i < PanelBufferSize; i++)
            {
                GameObject panel = Instantiate(contentPanelPrefab, parentRectTransform);
                ContentPanel newPanel = panel.GetComponent<ContentPanel>();
                RectTransform panelRect = newPanel.Rect;

                // Position the panel
                panelRect.anchorMin = new Vector2(0, 0.5f);
                panelRect.anchorMax = new Vector2(1, 0.5f);
                panelRect.pivot = new Vector2(0.5f, 0.5f);
                panelRect.sizeDelta = new Vector2(0, parentRectTransform.rect.height);
                panelRect.anchoredPosition = new Vector2(0, (middleIndex - i) * parentRectTransform.rect.height);

                // Add to the swipe manager
                bufferPanels.Add(newPanel);


                int offset = i - middleIndex;
                int index;

                if (offset >= 0)
                {
                    // For the middle element and the elements to its right
                    index = offset;
                }
                else
                {
                    // For the elements to the left of the middle element
                    index = (1000 + offset) % 1000;
                }

                newPanel.SetContent(index);
            }

            
            // Tell SwipeManager Booting has finished //
            swipeManager.Initialize(bufferPanels);
            
        }
    }

}