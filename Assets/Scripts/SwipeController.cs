using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;


namespace DancePants
{
    //todo (refactor): this class is doing a bit too much. might be good idea to separate drag and panel buffering responsabilities into separate scripts.

    /// <summary>
    /// Class that detects drag events and moves panels up and down.
    /// ReShuffles panels to keep a buffer of 2 panels above and 2 panels below focused panel.
    /// Keeps track of visiblity calcuation, which is used to trigger all kinds of gameplay events (music, dancing anim, cleanup) 
    /// </summary>
    public class SwipeController : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        [Header("container for Content Panels")]
        public RectTransform panelContainer;

        private List<ContentPanel> _contentPanels;
        private Vector2 _startDragPosition;
        private Vector2 _startAnchoredPosition;
        public int _contentIndex = 0;
        private float _dragStartTime;
        private static int _maxContentIndex => ResourceManager.DanceSessionsCount;
        private int midpoint => _contentPanels.Count / 2;
        public void Initialize(List<ContentPanel> panels)
        {
            _contentPanels = panels;
            StartCoroutine(SnapToPosition());
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _startDragPosition = eventData.position;
            _startAnchoredPosition = panelContainer.anchoredPosition;
            _dragStartTime = Time.unscaledTime;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            float dragDelta = _startDragPosition.y - eventData.position.y;

            // Measure the swipe speed
            float swipeDuration = Time.unscaledTime - _dragStartTime;
            float swipeSpeed = Mathf.Abs(dragDelta) / swipeDuration;

            // Dynamic Threshold Calculation
            // This dynamic threshold changes its value based on swipe speed and screen size.  
            // Different devices have different swipe "feel" , so this allows some calibration to happen in runtime. 
            // todo: it depends on a few magic numbers, and still requires extensive playtesting to get it "just right".
            float screenHeight = Screen.height;
            float baseThreshold = 25; // todo: play with these numbers
            float swipeSpeedFactor = Mathf.Clamp(swipeSpeed / 1000, 0.5f, 2.5f); // todo: play with these numbers
            float screenFactor = 1200f / screenHeight; // 1200 is the reference height. 
            float dynamicThreshold = Mathf.Clamp(baseThreshold * swipeSpeedFactor * screenFactor, baseThreshold, 75);

            // Measure of how much of the panel is visible on-screen.
            float visibility = (1 - Mathf.Abs(dragDelta) / panelContainer.rect.height) * 100f;

            
            // if threshold is met, the swipe will complete on its own and Snap the next panel into place. 
            // i.e, swiping has a "point of no return" beyond the threshold, otherwise it snaps back to pre-swipe position.
            if (visibility <= dynamicThreshold)
            {
                if (dragDelta < 0)
                {
                    // Swiping up
                    _contentIndex = (_contentIndex + 1) % _maxContentIndex;
                    MovePanelToBottom();
                }
                else
                {
                    // Swiping down
                    _contentIndex = (_contentIndex - 1 + _maxContentIndex) % _maxContentIndex;
                    MovePanelToTop();
                }
            }

            StartCoroutine(SnapToPosition());
        }


        private void MovePanelToTop()
        {
            // Take the bottom panel and move it to the top
            ContentPanel panelToMove = _contentPanels[^1];
            _contentPanels.RemoveAt(_contentPanels.Count - 1);
            _contentPanels.Insert(0, panelToMove);

            RectTransform panelRect = panelToMove.GetComponent<RectTransform>();
            RectTransform formerFirstPanelRect = _contentPanels[1].GetComponent<RectTransform>();

            // Set the position of the moved panel
            panelRect.anchoredPosition =
                new Vector2(0, formerFirstPanelRect.anchoredPosition.y + panelContainer.rect.height);

            // Update content of the moved panel
            int newIndex = (_contentPanels[1].contentId - 1 + _maxContentIndex) % _maxContentIndex;

            panelToMove.SetContent(newIndex);
            panelToMove.transform.SetAsFirstSibling();
        }

        private void MovePanelToBottom()
        {
            // Take the top panel and move it to the bottom
            ContentPanel panelToMove = _contentPanels[0];
            _contentPanels.RemoveAt(0);
            _contentPanels.Add(panelToMove);

            RectTransform panelRect = panelToMove.GetComponent<RectTransform>();
            RectTransform formerLastPanelRect = _contentPanels[^2].GetComponent<RectTransform>()
                .GetComponent<RectTransform>();

            // Set the position of the moved panel
            panelRect.anchoredPosition =
                new Vector2(0, formerLastPanelRect.anchoredPosition.y - panelContainer.rect.height);

            // Update content of the moved panel
            int newIndex = (_contentPanels[^2].contentId + 1) % _maxContentIndex;
            panelToMove.SetContent(newIndex);
            panelToMove.transform.SetAsLastSibling();
        }

        public void OnDrag(PointerEventData eventData)
        {
            float newY = _startAnchoredPosition.y + (eventData.position.y - _startDragPosition.y);
            panelContainer.anchoredPosition = new Vector2(0, newY);

            RecalculateVisibility();
        }

        private IEnumerator SnapToPosition()
        {
            float elapsedTime = 0f;
            float duration = 0.25f; // You can adjust this to make the snapping faster or slower

            // Find the target panel based on contentId
            RectTransform targetPanel =
                _contentPanels.First(x => x.contentId == _contentIndex).GetComponent<RectTransform>();

            // Calculate the position needed to center the target panel
            float targetY = -(targetPanel.anchoredPosition.y - (panelContainer.rect.height / 2) +
                              (targetPanel.rect.height / 2));

            Vector2 startPosition = panelContainer.anchoredPosition;
            Vector2 targetPosition = new Vector2(0, targetY);

            // Lerp over the duration
            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                float t = elapsedTime / duration;

                //todo: try out different easings. might be fun for different avatars or music to have different panel easings.
                // Cubic ease in ease out
                t = t < 0.5f ? 4 * Mathf.Pow(t, 3) : 1 - Mathf.Pow(-2 * t + 2, 3) / 2;
                // Quadratic ease in
                //t = t * t;
                //Quadratic ease in ease out
                // t = t < 0.5f ? 2 * t * t : -2 * t * (t - 2) - 1;
                //Sine ease in
                //t = Mathf.Sin((t - 1) * Mathf.PI / 2) + 1;
                //Exponential ease in
                //t = Mathf.Pow(2, 10 * (t - 1));
                //Circular ease-in
                //t = 1 - Mathf.Sqrt(1 - (t * t));
                //Elastic ease out
                //t = Mathf.Pow(2, -10 * t) * Mathf.Sin((t - 0.075f) * (2 * Mathf.PI) / 0.3f) + 1;


                panelContainer.anchoredPosition = Vector2.Lerp(startPosition, targetPosition, t);

                yield return null;
            }

            // Snap to the exact position
            panelContainer.anchoredPosition = targetPosition;

            // Set the visibility of the center panel to 100 and others to 0
            for (int i = 0; i < _contentPanels.Count; i++)
            {
                _contentPanels[i].SetVisibility(i == midpoint ? 100f : 0f); 
            }
        }

        private void RecalculateVisibility()
        {
            float totalHeight = panelContainer.rect.height;
            float totalDrag = panelContainer.anchoredPosition.y - _startAnchoredPosition.y;

            // Determine the index of the current central panel.
            int dragPanels = Mathf.FloorToInt(Mathf.Abs(totalDrag) / totalHeight);
            int centralPanelIndex = _contentPanels.Count / 2; 
            if (totalDrag < 0)
            {
                centralPanelIndex -= dragPanels;
            }
            else
            {
                centralPanelIndex += dragPanels;
            }

            // Make sure the central panel index is within bounds
            if (centralPanelIndex < 0 || centralPanelIndex >= _contentPanels.Count)
            {
                return; // Exceeded bounds, ignore this drag event
            }

            ContentPanel currentPanel = _contentPanels[centralPanelIndex];
            ContentPanel nextPanel;

            float remainingDrag = Mathf.Abs(totalDrag) % totalHeight;

            if (totalDrag > 0)
            {
                nextPanel = (centralPanelIndex + 1) < _contentPanels.Count ? _contentPanels[centralPanelIndex + 1] : null;
            }
            else // dragging up
            {
                nextPanel = (centralPanelIndex - 1) >= 0 ? _contentPanels[centralPanelIndex - 1] : null;
            }

            float visibleHeightCurrent = totalHeight - remainingDrag;
            float visibleHeightNext = remainingDrag;

            float visibilityCurrent = (visibleHeightCurrent / totalHeight) * 100f;
            float visibilityNext = (visibleHeightNext / totalHeight) * 100f;

            // Reset visibility for all panels
            foreach (var panel in _contentPanels)
            {
                panel.SetVisibility(0f);
            }

            // Set visibility for current and next panel
            currentPanel.SetVisibility(visibilityCurrent);
            if (nextPanel != null)
            {
                nextPanel.SetVisibility(visibilityNext);
            }
        }
    }
}