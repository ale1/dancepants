using System;
using System.Collections;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace DancePants
{
    public class ContentPanel : MonoBehaviour
    {
        public int contentId;
        [SerializeField] private TextMeshProUGUI visibilityText;
        [SerializeField] private Transform DancerContainer;

        public RectTransform Rect => GetComponent<RectTransform>();
        
        private Dancer _dancer;
        
        
        // Start is called before the first frame update
        void Start()
        {

        }

        private IEnumerator LoadAvatarAsync(string avatarName, Action<GameObject> callback)
        {
            // Begin loading the asset asynchronously
            var path = "Avatars/" + avatarName;
            ResourceRequest request = Resources.LoadAsync<GameObject>(path);

            // Wait until the asset is fully loaded
            yield return new WaitUntil(() => request.isDone);

            // return the loaded asset through callback
            GameObject avatarPrefab = request.asset as GameObject;
            
            if(avatarPrefab == null)
                Debug.LogError($"{path}avatarPrefab not found");
            
            callback?.Invoke(avatarPrefab);
        }

        private IEnumerator LoadSongAsync(string songName, Action<AudioClip> callback)
        {
            // Begin loading the asset asynchronously
            var path = "Songs/" + songName;
            ResourceRequest request = Resources.LoadAsync<AudioClip>(path);

            // Wait until the asset is fully loaded
            yield return new WaitUntil(() => request.isDone);

            // return the loaded asset through callback
            AudioClip clip = request.asset as AudioClip;
            
            if(clip == null)
                Debug.LogError($"{path} clip not found");
            
            callback?.Invoke(clip);
            
        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Show this panel's visiblity on the UI, and pass the vis value to the dancer 
        /// </summary>
        /// <param name="vis"> percentage between 0 and 100</param>
        public void SetVisibility(float vis)
        {
            _dancer.SetVisibility(vis);
            visibilityText.text = Mathf.Round(vis).ToString(CultureInfo.InvariantCulture);
        }

        public void SetContent(int id)
        {
            // clear old content
            if(_dancer)
                Destroy(_dancer.gameObject);

            this.contentId = id;
            this.gameObject.name = "panel_" + id;

            var background = GetComponent<Image>();
            if (background != null)
                background.color = ResourceManager.GetBackgroundById(contentId);
            
            bool isAvatarLoaded = false;
            bool isSongLoaded = false;
            GameObject avatarPrefab = null;
            AudioClip songClip = null;

            var avatarName = ResourceManager.GetAvatarNameById(contentId);
            StartCoroutine(LoadAvatarAsync(avatarName, OnAvatarLoaded));

            var songName = ResourceManager.GetSongNameById(contentId);
            StartCoroutine(LoadSongAsync(songName, OnSongLoaded));

            void OnAvatarLoaded(GameObject loadedAvatarPrefab)
            {
                avatarPrefab = loadedAvatarPrefab;
                isAvatarLoaded = true;
                TrySetupAvatar();
            }

            void OnSongLoaded(AudioClip loadedClip)
            {
                songClip = loadedClip;
                isSongLoaded = true;
                TrySetupAvatar();
            }

            void TrySetupAvatar()
            {
                // If both avatar and song are loaded, execute final setup
                if (isAvatarLoaded && isSongLoaded)
                {
                    _dancer = GameObject.Instantiate(avatarPrefab, DancerContainer.transform).GetComponent<Dancer>();
                    _dancer.SetSong(songClip);
                }
            }
        }
    }
}