using UnityEngine;


namespace DancePants
{
    /// <summary>
    /// A simple example of a class holding all the data necessary to load and generate a contentPanel
    /// </summary>
    public struct DanceSession
    {
        public string PlayerId; // the player who created this dance
        public string AvatarName; //The avatar used in this dance
        public string SongName; // The song being played for this dance
        public float[] StageName; // the background stage decor for this dance. todo: replace colored background with funky textures.
    }
}