using UnityEngine;
using UnityEngine.UI;


namespace DancePants
{
    /// <summary>
    /// This class generates a render-texture on the fly, which is then linked ot the DanceStudio Cam and the corresponding Projection GO.
    /// The intended effect is a "Live TV studio" type setup where dancer is off-screen being recorded by a separate camera from the main camera.
    /// The live recording is projected unto the Canvas UI. There is some magic to size the raw image and render texture in such as way that it preserves scale (no deformity) while fitting nicely inside our panel UI.  
    /// </summary>
    public class AssignRenderTextureToRawImage : MonoBehaviour
    {
        [SerializeField] private RawImage rawImage;
        [SerializeField] private Camera renderCamera;
        [SerializeField] private int textureHeight = 1024;

        private bool Landscape => Screen.width > Screen.height;

        void Start()
        {
            // Determine the aspect ratio of the camera
            float aspectRatio = renderCamera.aspect;

            // Calculate the width based on the height while maintaining the aspect ratio
            int textureWidth = Mathf.RoundToInt(textureHeight * aspectRatio);

            // Create a new RenderTexture
            RenderTexture renderTexture = new RenderTexture(textureWidth, textureHeight, 16);
            renderTexture.Create();

            // Assign the RenderTexture to the RawImage
            rawImage.texture = renderTexture;

            // Assign the RenderTexture to the Camera
            renderCamera.targetTexture = renderTexture;

            // Get parent RectTransform size to use as reference max size (with some margins)
            RectTransform parentRectTransform = rawImage.transform.parent.GetComponent<RectTransform>();
            float maxImageWidth = parentRectTransform.rect.width * (Landscape ? 0.7f : 0.9f);
            float maxImageHeight = parentRectTransform.rect.height * (Landscape ? 0.9f : 0.7f);

            // Calculate appropriate size maintaining aspect ratio
            float scale = Mathf.Min(maxImageWidth / textureWidth, maxImageHeight / textureHeight);
            float newWidth = textureWidth * scale;
            float newHeight = textureHeight * scale;

            // Set size on RawImage's RectTransform
            RectTransform rawImageRectTransform = rawImage.GetComponent<RectTransform>();
            rawImageRectTransform.sizeDelta = new Vector2(newWidth, newHeight);
        }
    }
}