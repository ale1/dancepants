using System.Collections.Generic;
using UnityEngine;


namespace DancePants
{
    /// <summary>
    ///  This class holds a dictionary with the 1000 mocked DanceSessions.
    ///  Its just for Bootstrapping reasons to simulate fetching content from an external source.
    ///  In real-life, this would be replaced by much more complex system using AssetBundles or Addressables or content delivery service, etc
    /// </summary>
    public class ResourceManager : MonoBehaviour
    {
        public static int DanceSessionsCount => 1000; // simulates the amount of DanceSessions stored that are available to this player.
        
        public static Dictionary<int, DanceSession> DanceSessions = new();

        private static ResourceManager instance;

        private void Awake()
        {
            instance = this;
        }

        // Start is called before the first frame update
        void Start()
        {
            
        }

        public static string GetAvatarNameById(int contentId)
        {
            if (DanceSessions.TryGetValue(contentId, out DanceSession value))
            {
                return value.AvatarName;
            }
            else
            {
                Debug.LogError($"ResourceManager ::: avatarName not found for id {contentId}");
                return "Avatar1";
            }
        }

        public static string GetSongNameById(int contentId)
        {
            if (DanceSessions.TryGetValue(contentId, out DanceSession value))
            {
                return value.SongName;
            }
            else
            {
                Debug.LogError($"ResourceManager ::: songName not found for id {contentId}");
                return "Song1";
            }
        }

        public static Color GetBackgroundById(int contentId)
        {
            if (DanceSessions.TryGetValue(contentId, out DanceSession value))
            {
                var rgba = value.StageName;
                return new Color(rgba[0], rgba[1], rgba[2], rgba[3]);
            }
            else
            {
                Debug.LogError($"ResourceManager ::: songName not found for id {contentId}");
                return Color.black;
            }
        }

    }
}
